defmodule RNATranscription do
  @dna_to_rna_map %{?G => ?C, ?C => ?G, ?T => ?A, ?A => ?U}


  @doc """
  Transcribes a character list representing DNA nucleotides to RNA

  ## Examples

  iex> RNATranscription.to_rna('ACTG')
  'UGAC'
  """
  @spec to_rna([char]) :: [char]
  def to_rna(dna), do: to_rna(dna, [])

  @spec to_rna([], [char]) :: [char]
  defp to_rna([], rna), do: rna |> Enum.reverse

  @spec to_rna([char], [char]) :: [char]
  defp to_rna([dna | dna_tail], rna), do: to_rna(dna_tail, [@dna_to_rna_map[dna]|rna])

end
